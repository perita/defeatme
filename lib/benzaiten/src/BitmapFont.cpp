//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // HAVE_BENZAITEN_CONFIG_H
#include "BitmapFont.hpp"
#include <algorithm>
#include <iterator>
#include <string>
#include <vector>

using namespace benzaiten;

namespace
{
    ///
    /// @brief Converts an ASCII string to wide string.
    ///
    /// @param[in] str The string to convert.
    /// @return A wide string with the same representation as @p str.
    ///
    std::wstring
    string2wstring (const std::string &str)
    {
        std::vector<wchar_t> wstr;
        wstr.reserve (str.size () + 1);
        std::copy (str.begin (), str.end (), std::back_inserter (wstr));
        wstr.push_back (L'\0');
        return std::wstring (&wstr[0]);
    }
}

BitmapFont::BitmapFont (const std::wstring &characters, const Surface &surface):
    characters_ (),
    surface_ (surface)
{
    const uint32_t separator = surface.getPixel (0, 0);

    uint16_t x = 1;
    std::wstring::const_iterator character = characters.begin ();
    while ( x < surface_.width () && character != characters.end () )
    {
        if ( separator != surface_.getPixel (x, 0) )
        {
            SDL_Rect rect;
            rect.x = x;
            rect.y = 0;
            rect.h = surface.height ();
            rect.w = 0;
            while ( (x < surface_.width ()) &&
                    (separator != surface_.getPixel (x, 0)) )
            {
                ++x;
            }
            rect.w = x - rect.x;
            characters_[*character] = rect;
            ++character;
        }
        ++x;
    }
}

SDL_Rect
BitmapFont::draw (const std::wstring &text, int16_t x, int16_t y,
        Surface &screen) const
{
    SDL_Rect rect = {x, y, 0, height ()};

    const int16_t startX = x;
    for ( std::wstring::const_iterator character = text.begin () ;
            character != text.end () ; ++character )
    {
        if ( L'\n' == *character )
        {
            rect.w = std::max (uint16_t(rect.w), uint16_t (x - rect.x));
            rect.h += height ();

            x = startX;
            y += height ();
        }
        else
        {
            std::map<std::wstring::value_type, SDL_Rect>::const_iterator item =
                characters_.find (*character);
            if ( item != characters_.end () )
            {
                const SDL_Rect &rect (item->second);
                surface_.blit (rect.x, rect.y, rect.w, rect.h, x, y, screen);
                x += rect.w;
            }
        }
    }

    rect.w = std::max (uint16_t(rect.w), uint16_t (x - rect.x));
    return rect;
}

SDL_Rect
BitmapFont::draw (const std::string &text, int16_t x, int16_t y,
        Surface &screen) const
{
    return draw (string2wstring (text), x, y, screen);
}

uint16_t
BitmapFont::height () const
{
    return surface_.height ();
}

uint16_t
BitmapFont::width (const std::wstring &text) const
{
    uint16_t totalWidth = 0;
    uint16_t width = 0;
    for ( std::wstring::const_iterator character = text.begin () ;
            character != text.end () ; ++character )
    {
        if ( L'\n' != *character )
        {
            std::map<std::wstring::value_type, SDL_Rect>::const_iterator item =
                characters_.find (*character);
            if ( item != characters_.end () )
            {
                width += item->second.w;
            }
        }
        else
        {
            totalWidth = std::max (width, totalWidth);
            width = 0;
        }
    }

    return std::max (width, totalWidth);
}

uint16_t
BitmapFont::width (const std::string &text) const
{
    return width (string2wstring (text));
}
