//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_EIGHT_WAY_CONTROLLER_HPP)
#define GEISHA_STUDIOS_BENZAITEN_EIGHT_WAY_CONTROLLER_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include "Utils.hpp"

#if !defined(_MSC_VER)
#include <stdint.h>
#endif // !_MSC_VER

#define BEGIN_MOVE_OF(object, up, upRight, right, downRight, \
        down, downLeft, left, upLeft) \
        case up: \
            object.beginMoveUp(); \
            break; \
\
        case upRight: \
            object.beginMoveUp(); \
            object.beginMoveRight(); \
            break; \
\
        case right: \
            object.beginMoveRight(); \
            break; \
\
        case downRight: \
            object.beginMoveDown(); \
            object.beginMoveRight(); \
            break; \
\
        case down: \
            object.beginMoveDown(); \
            break; \
\
        case downLeft: \
            object.beginMoveDown(); \
            object.beginMoveLeft(); \
            break; \
\
        case left: \
            object.beginMoveLeft(); \
            break; \
 \
        case upLeft: \
            object.beginMoveUp(); \
            object.beginMoveLeft(); \
            break

#define END_MOVE_OF(object, up, upRight, right, downRight, \
        down, downLeft, left, upLeft) \
        case up: \
            object.endMoveUp(); \
            break; \
\
        case upRight: \
            object.endMoveUp(); \
            object.endMoveRight(); \
            break; \
\
        case right: \
            object.endMoveRight(); \
            break; \
\
        case downRight: \
            object.endMoveDown(); \
            object.endMoveRight(); \
            break; \
\
        case down: \
            object.endMoveDown(); \
            break; \
\
        case downLeft: \
            object.endMoveDown(); \
            object.endMoveLeft(); \
            break; \
\
        case left: \
            object.endMoveLeft(); \
            break; \
 \
        case upLeft: \
            object.endMoveUp(); \
            object.endMoveLeft(); \
            break

namespace benzaiten
{
    class Animation;

    template<class Super> class EightWayController: public Super
    {
        public:
            EightWayController(const Animation &animation, int16_t x,
                    int16_t y, float speed):
                Super(animation, x, y, speed),
                direction_()
            {
            }

            void beginMoveDown()
            {
                direction_.y += Down;
            }

            void beginMoveLeft()
            {
                direction_.x += Left;
            }

            void beginMoveRight()
            {
                direction_.x += Right;
            }

            void beginMoveUp()
            {
                direction_.y += Up;
            }

            void endMoveDown()
            {
                direction_.y -= Down;
            }

            void endMoveLeft()
            {
                direction_.x -= Left;
            }

            void endMoveRight()
            {
                direction_.x -= Right;
            }

            void endMoveUp()
            {
                direction_.y -= Up;
            }

            void update(float elapsedTime, int16_t minX, int16_t maxX,
                    int16_t minY, int16_t maxY)
            {
                float speed = Super::speed();
                // In diagonal, we need to go slower.
                // Around sin(45) = cos(45) = 0.7071, actually.
                if (None != direction_.x && None != direction_.y)
                {
                    speed *= 0.7071f;
                }

                if (None != direction_.x)
                {
                    Super::setX(clamp(
                            Super::x() + (direction_.x > 0 ? speed : -speed),
                            float(minX), float(maxX)));
                }
                if (None != direction_.y)
                {
                    Super::setY(clamp(
                            Super::y() + (direction_.y > 0 ? speed : -speed),
                            float(minY), float(maxY)));
                }

                Super::update(elapsedTime);
            }

        private:
            enum Dir
            {
                Down = 1,
                Left = -1,
                None = 0,
                Right = 1,
                Up = -1
            };

            struct Direction
            {
                int x;
                int y;

                Direction(): x(None), y(None) {}
            };

            Direction direction_;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_EIGHT_WAY_CONTROLLER_HPP
