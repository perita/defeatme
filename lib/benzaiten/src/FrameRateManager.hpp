//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_FRAME_RATE_MANAGER_HPP)
#define GEISHA_STUDIOS_BENZAITEN_FRAME_RATE_MANAGER_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#if !defined(_MSC_VER)
#include <stdint.h>
#else // _MSC_VER
#include <SDL.h> // includes ?int*_t defintions for _MSC_VER.
#endif // !_MSC_VER


namespace benzaiten
{
    ///
    /// @class FrameRateManager.
    /// @brief Tries to keep the game to a fixed frame rate.
    ///
    class FrameRateManager
    {
        public:
            ///
            /// @brief Initializes the frame rate with the frame rate to keep.
            ///
            /// @param[in] frameRate The frame rate to try to keep.
            ///
            FrameRateManager (float frameRate);

            ///
            /// @brief Gets milliseconds between two last calls to update().
            ///
            /// @return The milliseconds between the two last consecutive calls
            ///         to update().
            ///
            uint32_t elapsedTime () const;

            ///
            /// @brief Updates the frame manager.
            ///
            /// If the time elapsed from the last call to this function is
            /// not the expected time for the frame rate to keep, this
            /// function makes the application wait the time necessary to
            /// keep the expected frame rate.
            ///
            void update ();

        private:
            /// The number of milliseconds from the last call to update().
            uint32_t elapsedTime_;
            /// The frame rate to try to keep.
            uint32_t frameRate_;
            /// The number of clock tocks from the last update.
            uint32_t lastTicks_;
            /// The mean elapsed time from the previous period.
            uint32_t meanElapsedTime_;
            /// The number of times the update() function was called.
            uint32_t numUpdates_;
            /// The number of elapsed milliseconds from the last mean.
            uint32_t totalElapsedTime_;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_FRAME_RATE_MANAGER_HPP
