//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_GAME_HPP)
#define GEISHA_STUDIOS_BENZAITEN_GAME_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <stdexcept>
#include <boost/noncopyable.hpp>
#include "FrameRateManager.hpp"

// Forward declarations.
class string;

namespace benzaiten
{
    // Forward declarations.
    class EventManager;
    class GameStateManager;
    class Surface;

    ///
    /// @class Game
    /// @brief The main game's system class.
    ///
    /// This class is intended to be used as an stack object in main().
    ///
    class Game: public boost::noncopyable
    {
        public:
            ///
            /// @brief Constructor.
            ///
            /// Initializes the game's environment to run properly.  The
            /// game won't start until the run() member function is called.
            ///
            /// @param[in] eventManager The reference to the event manager to
            ///            use.
            /// @param[in] stateManager The game event manager to use.  It
            ///            must contain at least a single state or the game
            ///            will quit immediately.
            /// @param[in] screen The screen surface where to draw the active
            ///            state to.
            /// @param[in] fps The desired frames per second.
            ///
            ///
            Game (EventManager &eventManager, GameStateManager &stateManager,
                    Surface &screen, float fps = 35.0f);

            ///
            /// @brief Starts the execution of the game's loop.
            ///
            /// The system must already be initialized before calling
            /// this function.
            ///
            /// @throw std::exception If there's any exception.
            ///
            void run ();

        private:
            ///
            /// @brief The game received a Quit event.
            ///
            /// This will just call done(true) and finish the game as soon
            /// as possible (without losing changes.)
            ///
            void onQuit ();

            ///
            /// @brief Tells whether the game is finished.
            ///
            /// @return @c true if the game is finished, @c false otherwise.
            ///
            bool over () const;

            ///
            /// @brief Sets whether the game is finished.
            ///
            /// @param[in] isDone Set it to @c true to finish the game, @c false
            ///            to continue.
            ///
            void over (bool isDone);

            ///
            /// @brief Runs the inner game's loop once.
            ///
            /// The system must already be initialized before calling
            /// this function.
            ///
            /// @throw std::exception If there's any exception.
            ///
            void runOnce ();

            ///
            /// @brief Runs the inner game's loop once.
            ///
            /// The system must already be initialized before calling
            /// this function.
            ///
            /// @param[in] game Pointer to the Game instance to run.
            ///
            /// @throw std::exception If there's any exception.
            ///
            static void runOnce (void *game);


            /// Tells if the game is finished.
            bool over_;
            EventManager &eventManager_;
            GameStateManager &stateManager_;
            Surface &screen_;
            FrameRateManager frameRateManager_;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_GAME_HPP
