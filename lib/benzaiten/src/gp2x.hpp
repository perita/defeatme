//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_GP2X_HPP)
#define GEISHA_STUDIOS_BENZAITEN_GP2X_HPP

#define GP2X_BUTTON_UP (0)
#define GP2X_BUTTON_DOWN (4)
#define GP2X_BUTTON_LEFT (2)
#define GP2X_BUTTON_RIGHT (6)
#define GP2X_BUTTON_UPLEFT (1)
#define GP2X_BUTTON_UPRIGHT (7)
#define GP2X_BUTTON_DOWNLEFT (3)
#define GP2X_BUTTON_DOWNRIGHT (5)
#define GP2X_BUTTON_CLICK (18)
#define GP2X_BUTTON_A (12)
#define GP2X_BUTTON_B (13)
#define GP2X_BUTTON_X (14)
#define GP2X_BUTTON_Y (15)
#define GP2X_BUTTON_L (10)
#define GP2X_BUTTON_R (11)
#define GP2X_BUTTON_START (8)
#define GP2X_BUTTON_SELECT (9)
#define GP2X_BUTTON_VOLUP (16)
#define GP2X_BUTTON_VOLDOWN (17)

#endif // !GEISHA_STUDIOS_BENZAITEN_GP2X_HPP
