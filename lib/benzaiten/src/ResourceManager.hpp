//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_RESOURCE_MANAGER_HPP)
#define GEISHA_STUDIOS_BENZAITEN_RESOURCE_MANAGER_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <cstddef>
#include <map>
#include <stdexcept>
#include <string>
#include <boost/noncopyable.hpp>
#include <boost/function.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include "MusicMixer.hpp"
#include "SoundMixer.hpp"
#include "Surface.hpp"

namespace benzaiten
{
    ///
    /// @class ResourceManager
    /// @brief Manages the resources needed by other classes (surface, etc.)
    ///
    class ResourceManager: boost::noncopyable
    {
        public:
            typedef boost::function<SDL_Surface *(const SDL_Surface *)> GraphicScaleFun;

            ///
            /// @brief Constructor.
            ///
            /// Gets the more addequate directory from where to get the
            /// resources later based on the platform.
            ///
            /// @param[in] appName The name of the application.
            ///
            ResourceManager (const std::string &appName);

            ///
            /// @brief Gets the path to a resource file in a directory.
            ///
            /// @param[in] directory The directory where the file is located at.
            /// @param[in] fileName The name of the file to get its path in
            ///            the resource @p directory.
            /// @return The path to the resource files.
            ///
            std::string getFilePath (const std::string &directory,
                    const std::string &fileName) const;

            ///
            /// @brief Loads a graphic file.
            ///
            /// @param[in] fileName The file name of the graphic resource to
            ///            load.
            /// @return The loaded Surface.
            ///
            /// @throw std::invalid_argument If the resource couldn't be loaded.
            ///
            Surface graphic (const std::string &fileName) const;

            ///
            /// @brief Loads a music file.
            ///
            /// @param[in] fileName The file name of the music resource to load.
            /// @return The music loaded.
            ///
            /// @throw std::invalid_argument If the resource couldn't be loaded.
            ///
            MusicMixer music (const std::string &fileName) const;

            ///
            /// @brief Sets the function to use to scale the graphics.
            ///
            /// When loading graphics with graphic(), ResourceManager will
            /// use the function specified here, if any, to scale the newly
            /// loaded graphic.
            ///
            /// Setting the scale function will reset the weak pointers
            /// to already loaded resources, so requesting a previously
            /// loaded graphic will actually load it.
            ///
            /// @param[in] function The function to set as the scale function.
            ///            If the function is empty, then graphic() will not
            ///            try to rescale the graphic.
            ///
            void setGraphicScaleFunction (GraphicScaleFun function);

            ///
            /// @brief Loads a sound file.
            ///
            /// @param[in] fileName The file name of the sound resource to
            ///            load.
            /// @return The loaded sound.
            ///
            /// @throw std::invalid_argument If the resource couldn't be loaded.
            ///
            SoundMixer sound (const std::string &fileName) const;

            ///
            /// @brief Try to load a music file.
            ///
            /// @param[in] fileName The file name of the music resource to try
            ///            to load.
            /// @return A pointer to an actual implementation of a sound object if
            ///         @p fileName could be loaded.  A pointer to an empty
            ///         implementation otherwise.
            ///
            Music::ptr try_music(const std::string &fileName) const;

            ///
            /// @brief Try to load a sound file.
            ///
            /// @param[in] fileName The file name of the source resource to try
            ///            to load.
            /// @return A pointer to an actual implementation of a sound object if
            ///         @p fileName could be loaded.  A pointer to an empty
            ///         implementation otherwise.
            ///
            Sound::ptr try_sound(const std::string &fileName) const;

        private:
            /// A graphic resource.
            typedef boost::weak_ptr<SDL_Surface> Graphic;
            /// The pointer to the graphic resource.
            typedef boost::shared_ptr<SDL_Surface> GraphicPtr;
            /// A music resource.
            typedef boost::weak_ptr<Mix_Music> MusicRes;
            /// The pointer to the music resource.
            typedef boost::shared_ptr<Mix_Music> MusicPtr;
            /// A sound resource.
            typedef boost::weak_ptr<Mix_Chunk> SoundRes;
            /// The pointer to the sound resource.
            typedef boost::shared_ptr<Mix_Chunk> SoundPtr;

            ///
            /// @brief Finds where the resources path is.
            ///
            /// Using known information about the platform, finds which is
            /// the best suited resources path.
            ///
            /// @param appName The name of the application.
            ///
            /// @return The best resources path, relative to the current
            ///         working directory or absolute, where the resources
            ///         are located.
            ///
            static std::string findResourcesPath (const std::string &appName);

            ///
            /// @brief Gets the path to the resources files.
            ///
            /// @return The path to the resources files.
            ///
            std::string getResourcesPath () const;

            ///
            /// @brief Join two path depending on the platform.
            ///
            /// @param[in] path0 The first path to join.
            /// @param[in] path1 The second path to join.
            ///
            /// @return "path0/path1" or "path0\\path1" depending on the
            ///         platform.
            ///
            static std::string joinPath (const std::string &path0,
                    const std::string &path1);

            ///
            /// @brief Loads a graphic file.
            ///
            /// @param[in] fileName The name of the file to load.
            ///
            /// @return The smart pointer to the loaded graphic resource.
            ///
            /// @throw std::invalid_argument If the graphic could not be loaded.
            ///
            static GraphicPtr loadGraphicFile (const std::string &fileName);

            ///
            /// @brief Loads a music file.
            ///
            /// @param[in] fileName The name of the file to load.
            ///
            /// @return The smart pointer to the loaded music resource.
            ///
            /// @throw std::invalid_argument If the music could not be loaded.
            ///
            static MusicPtr loadMusicFile (const std::string &fileName);

            ///
            /// @brief Loads a sound file.
            ///
            /// @param[in] fileName The name of the file to load.
            ///
            /// @return The smart pointer to the loaded sound resource.
            ///
            /// @throw std::invalid_argument If the sound could not be loaded.
            ///
            static SoundPtr loadSoundFile (const std::string &fileName);

            /// The loaded graphics.
            mutable std::map<std::string, Graphic> graphics_;
            /// The function to scale the loaded graphics with.
            GraphicScaleFun graphicScaleFun_;
            /// The loaded musics.
            mutable std::map<std::string, MusicRes> musics_;
            /// The path there the resources are.
            std::string resourcesPath_;
            /// The loaded sounds.
            mutable std::map<std::string, SoundRes> sounds_;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_RESOURCE_MANAGER_HPP
