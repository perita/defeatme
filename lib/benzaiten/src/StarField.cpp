//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // !HAVE_CONFIG_H
#include "StarField.hpp"
#include <algorithm>
#include <cassert>
#include <ctime>
#include <boost/bind.hpp>
#include <boost/foreach.hpp>

using namespace benzaiten;

StarField::StarField(size_t width, size_t height):
    height_(height),
    layers_(),
    // The value *must* be unsigned.
    randomGenerator_(static_cast<unsigned int>(std::time(0))),
    width_(width)
{
    assert (width > 0 && "The star field's width can't be 0");
    assert (height > 0 && "The star field's height can't be 0");
}

void
StarField::addLayer(size_t stars, const Surface::Color &color, int16_t speed)
{
    assert(stars > 0 && "The number of stars can't be 0.");

    RandomCoordinateGenerator randomX(randomGenerator_,
            boost::uniform_int<>(0, width_ - 1));
    RandomCoordinateGenerator randomY(randomGenerator_,
            boost::uniform_int<>(0, height_ - 1));
    layers_.push_back(Layer(color, speed, stars, randomX, randomY));
}

void
StarField::draw(Surface &screen)
{
    std::for_each(layers_.begin(), layers_.end(),
            boost::bind(&StarField::Layer::draw, _1, screen));
}

void
StarField::update(float elapsedTime)
{
    std::for_each(layers_.begin(), layers_.end(),
            boost::bind(&StarField::Layer::update, _1, elapsedTime,
                static_cast<int16_t>(height_)));
}


////////////////////////////////
// Layer
////////////////////////////////
StarField::Layer::Layer(const Surface::Color &color, int16_t speed,
        size_t numStars, RandomCoordinateGenerator &randomX,
        RandomCoordinateGenerator &randomY):
    background(1, 1, 32),
    color(1, 1, 32),
    speed(speed),
    stars()
{
    background.fill(0, 0, 0);
    this->color.fill(color);
    for (size_t star = 0 ; star < numStars ; ++star) {
        stars.push_back(Star(randomX(), randomY()));
    }
}

void
StarField::Layer::draw(Surface &screen)
{
    BOOST_FOREACH(const StarField::Star &star, stars)
    {
        if (star.yPrev > -1) {
	    background.blit(star.x, star.yPrev, screen);
        }
        if (star.y > -1) {
	    color.blit(star.x, star.y, screen);
        }
    }
}

void
StarField::Layer::update(float elapsedTime, int16_t height)
{
    BOOST_FOREACH(StarField::Star &star, stars)
    {
        star.yPrev = star.y;
        star.y += speed;
        if (star.y >= height) {
            star.y -= height;
        }
    }
}
