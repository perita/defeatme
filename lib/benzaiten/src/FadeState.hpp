//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_FADE_STATE_HPP)
#define GEISHA_STUDIOS_BENZAITEN_FADE_STATE_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#if !defined(_MSC_VER)
#include <stdint.h>
#endif // !_MSC_VER
#include "IGameState.hpp"
#include "Surface.hpp"

namespace benzaiten
{
    ///
    /// @class FadeState
    /// @brief Fades the screen to the next state.
    ///
    class FadeState: public IGameState
    {
        public:
            ///
            /// @brief Initializes the fade.
            ///
            /// @param[in] duration The fade's duration, in milliseconds.
            /// @param[in] red The red component of the fade.
            /// @param[in] green The green component of the fade.
            /// @param[in] blue The blue component of the fade.
            ///
            FadeState (float duration = 300.0f, uint8_t red = 0,
                    uint8_t green = 0, uint8_t blue = 0);

            virtual void draw (Surface &screen);
            virtual void onActivate ();
            virtual void update (float elapsedTime);

        protected:
            ///
            /// @brief Gets the current value of alpha.
            ///
            /// @return The current value of alpha.
            ///
            int16_t alpha () const;

            ///
            /// @brief Sets the value of alpha.
            ///
            /// @param[in] value The value to set to alpha.
            ///
            void alpha (int16_t value);

            ///
            /// @brief Creates the background image to use to fade.
            ///
            /// @return The pointer to the background image to use to fade.
            ///
            virtual Surface::ptr background () const = 0;

            ///
            /// @brief Tells if the fade is done.
            ///
            /// @return @c true if the fade is done, @c false otherwise.
            ///
            virtual bool done () const = 0;

            ///
            /// @brief Gets the fade duration.
            ///
            /// @return The fade's duration in milliseconds.
            ///
            float duration () const;

            ///
            /// @brief Updates the value of alpha based on the ellapsed time.
            ///
            /// @param[in] elapsedTime The time elapsed since the last update.
            ///
            virtual void updateAlpha (float elapsedTime) = 0;

        private:
            /// The current value of alpha.
            int16_t alpha_;
            /// The background to draw.
            Surface::ptr background_;
            /// The fade's blue component.
            uint8_t blue_;
            /// The fade's duration in millisecond.
            float duration_;
            /// The fade's green component.
            uint8_t green_;
            /// The fade's red component.
            uint8_t red_;
    };

    ///
    /// @class FadeInState
    /// @brief Fades in the screen to the next state.
    ///
    class FadeInState: public FadeState
    {
        public:
            ///
            /// @brief Initializes the fade.
            ///
            /// @param[in] state The state to fade to.
            /// @param[in] red The fade's red component.
            /// @param[in] green The fade's green component.
            /// @param[in] blue The fade's blue component.
            /// @param[in] time The time to use to fade, in milliseconds.
            ///
            FadeInState (IGameState::ptr state, uint8_t red, uint8_t green,
                    uint8_t blue, uint32_t time = 200):
                FadeState (time, red, green, blue),
                state_ (state)
            {
                alpha (0);
            }

        protected:
            virtual bool
            done () const
            {
                return alpha () > 255;
            }

            virtual Surface::ptr
            background () const
            {
                Surface screen (Surface::screen ());
                state_->draw (screen);
                return Surface::ptr (new Surface (screen));
            }

            virtual void
            updateAlpha (float elapsedTime)
            {
                alpha (alpha () +
                        static_cast<int16_t> (255 * elapsedTime / duration ()));
            }

        private:
            /// The state to fade to.
            IGameState::ptr state_;
    };

    ///
    /// @class FadeOutState
    /// @brief Fades out the screen to the next state.
    ///
    class FadeOutState: public FadeState
    {
        public:
            ///
            /// @brief Initializes the fade.
            ///
            /// @param[in] time The time to use to fade, in milliseconds.
            ///
            FadeOutState (uint32_t time = 200):
                FadeState (time)
            {
                alpha (255);
            }

            virtual Surface::ptr
            background () const
            {
                Surface screen (Surface::screen ());
                return Surface::ptr (new Surface (screen));
            }

            virtual bool
            done () const
            {
                return alpha () < 0;
            }

            virtual void
            updateAlpha (float elapsedTime)
            {
                alpha (alpha () -
                        static_cast<int16_t> (255 * elapsedTime / duration ()));
            }
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_FADE_STATE_HPP
