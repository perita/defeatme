//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // !HAVE_BENZAITEN_CONFIG_H
#include "SettingsImplOSX.hpp"
#include <vector>
#include <CoreFoundation/CFNumber.h>
#include <CoreFoundation/CFPreferences.h>
#include <CoreFoundation/CFString.h>

using namespace benzaiten;

SettingsImplOSX::SettingsImplOSX(const std::string &/*appName*/):
    SettingsBase::Impl()
{
}

int
SettingsImplOSX::getInteger(const std::string &section,
        const std::string &name, int defaultValue) const
{
    std::string fullName (section + "." + name);
    CFStringRef key = CFStringCreateWithCString (NULL, fullName.c_str (),
            kCFStringEncodingASCII);
    CFNumberRef tmpValue = static_cast<CFNumberRef> (
            CFPreferencesCopyAppValue (key, kCFPreferencesCurrentApplication));
    CFRelease (key);
    int value = defaultValue;
    if ( tmpValue )
    {
        CFNumberGetValue (tmpValue, kCFNumberIntType, &value);
        CFRelease (tmpValue);
    }
    return value;
}

std::string
SettingsImplOSX::getString(const std::string &section,
        const std::string &name, const std::string &defaultValue) const
{
    std::string fullName (section + "." + name);
    CFStringRef key = CFStringCreateWithCString (NULL, fullName.c_str (),
            kCFStringEncodingASCII);
    CFStringRef tmpValue = static_cast<CFStringRef>(
            CFPreferencesCopyAppValue(key, kCFPreferencesCurrentApplication));
    CFRelease(key);
    std::string value = defaultValue;
    if (tmpValue)
    {
        unsigned int valueLength =
            CFStringGetMaximumSizeForEncoding(CFStringGetLength(tmpValue),
                    kCFStringEncodingASCII) + 1;
        std::vector<char> valueString(valueLength, '\0');
        CFStringGetCString(tmpValue, &(valueString[0]), valueLength,
                kCFStringEncodingASCII);
        CFRelease(tmpValue);
        value = std::string(&(valueString[0]));
    }

    return value;
}

void
SettingsImplOSX::setInteger(const std::string &section,
        const std::string &name, int value)
{
    std::string fullName (section + "." + name);
    CFStringRef key = CFStringCreateWithCString (NULL, fullName.c_str (),
            kCFStringEncodingASCII);
    CFNumberRef tmpValue = CFNumberCreate (NULL, kCFNumberIntType, &value);
    CFPreferencesSetAppValue (key, tmpValue, kCFPreferencesCurrentApplication);
    CFRelease (tmpValue);
    CFRelease (key);
    CFPreferencesAppSynchronize (kCFPreferencesCurrentApplication);
}

void
SettingsImplOSX::setString(const std::string &section,
        const std::string &name, const std::string &value)
{
    std::string fullName(section + "." + name);
    CFStringRef keyName = CFStringCreateWithCString(NULL, fullName.c_str (),
            kCFStringEncodingASCII);
    CFStringRef tempValue = CFStringCreateWithCString(NULL, value.c_str (),
            kCFStringEncodingASCII);
    CFPreferencesSetAppValue(keyName, tempValue,
            kCFPreferencesCurrentApplication);
    CFRelease(tempValue);
    CFRelease(keyName);
    CFPreferencesAppSynchronize(kCFPreferencesCurrentApplication);
}
