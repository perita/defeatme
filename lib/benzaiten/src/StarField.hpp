//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_STAR_FIELD_HPP)
#define GEISHA_STUDIOS_BENZAITEN_STAR_FIELD_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <vector>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/variate_generator.hpp>
#include "Surface.hpp"

namespace benzaiten
{
    ///
    /// @class StarField
    /// @brief Shows an star filed with multiple star layers.
    ///
    class StarField
    {
        public:
            ///
            /// @brief Constructor.
            ///
            /// @param[in] width The width to limit the stars' position to.
            /// @param[in] height The height to limit the stars' position to.
            ///
            StarField(size_t width, size_t height);

            ///
            /// @brief Adds a new layer of stars.
            ///
            /// @param[in] stars The number of stars in the layer.
            /// @param[in] color The stars' color.
            /// @param[in] speed The stars' speed.
            ///
            void addLayer(size_t stars, const Surface::Color &color,
                    int16_t speed);

            ///
            /// @brief Draws the star field.
            ///
            /// @param[in] screen The surface to draw the field to.
            ///
            void draw(Surface &screen);

            ///
            /// @brief Updates the star field.
            ///
            /// @param[in] elapsedTime The time, in ms., since the last
            ///            update.
            ///
            void update(float elapsedTime);

        private:
            typedef boost::mt19937 RandomNumberGenerator;
            typedef boost::variate_generator<RandomNumberGenerator&, boost::uniform_int<> > RandomCoordinateGenerator;

            ///
            /// @struct Star
            /// @brief A star.
            ///
            struct Star
            {
                int16_t x; /// The X position of the star.
                int16_t y; /// The Y position of the star.
                int16_t yPrev; /// The previous Y position of the star.

                ///
                /// @brief Constructor.
                /// @param[in] x The initial position of the star.
                /// @param[in] y The initial position of the star.
                ///
                Star(int16_t x, int16_t y):
                    x(x),
                    y(y),
                    yPrev(y)
                {
                };
            };

            ///
            /// @struct Layer
            /// @brief A star layer.
            ///
            struct Layer
            {
		/// The layer’s background color
                Surface background;
                /// The layer's star color.
                Surface color;
                /// The layer's speed.
                int16_t speed;
                /// The stars in this layer.
                std::vector<Star> stars;

                ///
                /// @brief Constructor.
                ///
                /// @param[in] color The layer's color.
                /// @param[in] speed The layer's speed.
                /// @param[in] numStars The number of stars of this layer.
                /// @param[inout] randomX The random coordinate generator for X.
                /// @param[inout] randomY The random coordinate generator for Y.
                ///
                Layer(const Surface::Color &color, int16_t speed,
                        size_t numStars, RandomCoordinateGenerator &randomX,
                        RandomCoordinateGenerator &randomY);

                ///
                /// @brief Draws the layer.
                ///
                /// @param[in] screen The screen to draw the layer to.
                ///
                void draw(Surface &screen);

                ///
                /// @brief Updates the layer.
                ///
                /// @param[in] elapsedTime The elapsed time, in ms., since
                /// the last update.
                /// @param[in] height The height of where the stars must be
                ///            within.
                ///
                void update(float elapsedTime, int16_t height);
            };

            /// The height of the field.
            size_t height_;
            /// All the stars in layers.
            std::vector<Layer> layers_;
            /// The randon numeber generator.
            RandomNumberGenerator randomGenerator_;
            /// The width of the field.
            size_t width_;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_STAR_FIELD_HPP
