set(GFX_FILES
    ${CMAKE_CURRENT_SOURCE_DIR}/bullet.tga
    ${CMAKE_CURRENT_SOURCE_DIR}/enemy.tga
    ${CMAKE_CURRENT_SOURCE_DIR}/gameover.tga
    ${CMAKE_CURRENT_SOURCE_DIR}/icon.tga
    ${CMAKE_CURRENT_SOURCE_DIR}/logo.tga
    ${CMAKE_CURRENT_SOURCE_DIR}/numbers.tga
    ${CMAKE_CURRENT_SOURCE_DIR}/player.tga
    ${CMAKE_CURRENT_SOURCE_DIR}/stage.tga
    ${CMAKE_CURRENT_SOURCE_DIR}/start.tga
    CACHE INTERNAL "List of graphics files to install")
install(FILES ${GFX_FILES} DESTINATION ${RESOURCES_DIR}/gfx)
