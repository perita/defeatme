<?xml version='1.0' encoding='latin1'?>
<Wix xmlns='http://schemas.microsoft.com/wix/2006/wi'
     xmlns:gaming='http://schemas.microsoft.com/wix/GamingExtension'
     xmlns:util='http://schemas.microsoft.com/wix/UtilExtension'>
  <Product Id='{1D10DBFE-C1B3-4C17-AB17-EC4947BBF91B}' Name='Defeat Me!' Language='1033' Version='@VERSION@' Manufacturer='Geisha Studios' UpgradeCode='{5A66056B-58BC-41EC-84B0-9D943781986A}'>
    <Package Description='A shooting game against yourself' Manufacturer='Geisha Studios' InstallScope='perMachine' Keywords='shooting arcade game' InstallerVersion='200' Compressed='yes' Platform='x86'/>
    <Media Id='1' Cabinet='defeatme.cab' EmbedCab='yes'/>
    <Upgrade Id='{5A66056B-58BC-41EC-84B0-9D943781986A}'>
      <UpgradeVersion Minimum='@VERSION@' IncludeMinimum='no' OnlyDetect='yes' Language='1033' Property='NEWPRODUCTFOUND'/>
      <UpgradeVersion Minimum='1.0.0.0' IncludeMinimum='yes' Maximum='@VERSION@' IncludeMaximum='no' Language='1033' Property='UPGRADEFOUND'/>
    </Upgrade>
    <Condition Message="A later version of [ProductName] is already installed.  Setup will now exit.">
      Installed OR NOT NEWPRODUCTFOUND 
    </Condition>
    <Property Id='WIXUI_INSTALLDIR' Value='INSTALLFOLDER'/>
    <Property Id='WIXUI_EXITDIALOGOPTIONALCHECKBOX' Value='1'/>
    <Property Id='WIXUI_EXITDIALOGOPTIONALCHECKBOXTEXT' Value='Launch Defeat Me!'/>
    <Property Id='WixShellExecTarget' Value='[#defeatme.exe]'/>
    <CustomAction Id='LaunchGame' BinaryKey='WixCA' DllEntry='WixShellExec' Impersonate='yes'/>
    <UI>
      <UIRef Id='Benzaiten_WixUI_InstallDir'/>
      <UIRef Id='WixUI_ErrorProgressText'/>
      <Publish Dialog='BenzaitenExitDialog' Control='Finish' Event='DoAction' Value='LaunchGame'>WIXUI_EXITDIALOGOPTIONALCHECKBOX = 1 AND NOT Installed</Publish>
    </UI>
    <Icon Id="defeatme.ico" SourceFile='@CMAKE_SOURCE_DIR@/data/gfx/defeatme.ico'/>
    <Property Id="ARPPRODUCTICON" Value="defeatme.ico" />
    
    <Directory Id='TARGETDIR' Name='SourceDir' DiskId='1'>
      <Directory Id='ProgramFilesFolder' Name='PFiles'>
        <Directory Id='GeishaStudiosFolder' Name='Geisha Studios'>
          <Directory Id='INSTALLFOLDER' Name='DefeatMe'>
            <Component Id='DefeatMeBinaryComponent' Guid='{F05CD446-6599-454C-894F-1EAA776608C5}'>
              <File Source='@CMAKE_BINARY_DIR@/src/defeatme.exe' KeyPath='yes'>
                <gaming:Game Id='605A14F3-8188-449E-870B-B67DBCA9A31B'>
                  <gaming:PlayTask Name='Play!'/>
                  <gaming:SupportTask Name='Home Page' Address='http://www.geishastudios.com/games/defeatme/'/>
                </gaming:Game>
              </File>
            </Component>
            <Component Id='SDLBinaryComponent' Guid='{19F237FC-464A-41FD-BA00-C629C061CB93}'>
              <File Source='@CMAKE_SOURCE_DIR@/lib/benzaiten/3rdparty/SDL/SDL.dll' KeyPath='yes'/>
            </Component>
            <Component Id='SDLmixerBinaryComponent' Guid='{053981ee-43fa-4506-883b-6d15a37e791c}'>
              <File Source='@CMAKE_SOURCE_DIR@/lib/benzaiten/3rdparty/SDL_mixer/SDL_mixer.dll' KeyPath='yes'/>
              <File Source='@CMAKE_SOURCE_DIR@/lib/benzaiten/3rdparty/SDL_mixer/ogg.dll'/>
              <File Source='@CMAKE_SOURCE_DIR@/lib/benzaiten/3rdparty/SDL_mixer/vorbis.dll'/>
              <File Source='@CMAKE_SOURCE_DIR@/lib/benzaiten/3rdparty/SDL_mixer/vorbisfile.dll'/>
            </Component>
            <Directory Id='GraphicsFolder' Name='gfx'>
              <Component Id='DefeatMeGraphicsComponent' Guid='{7b4237df-1dbb-4425-8831-65daf0755293}'>
                <?foreach GRAPHIC in @GFX_FILES@?>
                <File Source='$(var.GRAPHIC)'/>
                <?endforeach?>
              </Component>
            </Directory>
            <Directory Id='MusicFolder' Name='music'>
              <Component Id='DefeatMeMusicComponent' Guid='{099D7802-EDA2-47DC-8FD4-3F51310A0051}'>
                <?foreach MUSIC in @MUSIC_FILES@?>
                <File Source='$(var.MUSIC)'/>
                <?endforeach?>
              </Component>
            </Directory>
            <Directory Id='SoundsFolder' Name='sfx'>
              <Component Id='DefeatMeSoundsComponent' Guid='{D2E0F6AA-4DAF-41D4-8E6B-042489463428}'>
                <?foreach SFX in @SFX_FILES@?>
                <File Source='$(var.SFX)'/>
                <?endforeach?>
              </Component>
            </Directory>
          </Directory>
        </Directory>
      </Directory>
      <Directory Id='ProgramMenuFolder' Name='PMenu'>
        <Directory Id='DefeatMeProgramMenuFolder' Name='Defeat Me!'>
          <Component Id='DefeatMeStartMenuShortcutComponent' Guid='{EBAC32A4-398E-42F5-B398-054CC52CF2C9}'>
            <Shortcut Id='DefeatMeStartMenuShortcut' Name='Defeat Me!' Description='Fight against youself' Target='[INSTALLFOLDER]defeatme.exe' WorkingDirectory='INSTALLFOLDER'/>
            <util:InternetShortcut Id='DefeatMeHomeShortcut' Directory='DefeatMeProgramMenuFolder' Name='Defeat Me!&apos; Home Page' Target='http://www.geishastudios.com/games/defeatme/'/>
            <RemoveFolder Id='DefeatMeProgramMenuFolder' On='uninstall'/>
            <RegistryValue Root='HKCU' Key='Software\Geisha Studios\Defeat Me' Name='StartMenuShortcut' Type='integer' Value='1' KeyPath='yes'/>
          </Component>
        </Directory>
      </Directory>
    </Directory>

    <InstallExecuteSequence>
      <RemoveExistingProducts After='InstallFinalize'/>
    </InstallExecuteSequence>

    <Feature Id='DefeatMe' Title='DefeatMe' Level='1' Display='expand' AllowAdvertise='no'>
      <ComponentRef Id='DefeatMeBinaryComponent'/>
      <ComponentRef Id='DefeatMeGraphicsComponent'/>
      <ComponentRef Id='DefeatMeStartMenuShortcutComponent'/>
      <ComponentRef Id='DefeatMeMusicComponent'/>
      <ComponentRef Id='DefeatMeSoundsComponent'/>
      <ComponentRef Id='SDLBinaryComponent'/>
      <ComponentRef Id='SDLmixerBinaryComponent'/>
    </Feature>
  </Product>
</Wix>
