#!/bin/sh
set -ex

rm -fr build
mkdir build
cd build

if [ "$1" = 'wasm' ]; then
	mkdir src
	cp -a ../data ../src/index.html src
	embuilder build boost_headers
	emcmake cmake -DCMAKE_CXX_FLAGS='-std=c++11 -sNO_DISABLE_EXCEPTION_CATCHING -sUSE_BOOST_HEADERS' -DCMAKE_EXE_LINKER_FLAGS='--preload-file data' ..
	emmake make -j4
else
	cmake -DCMAKE_CXX_FLAGS="-Wall" -DCMAKE_C_FLAGS="-Wall" -DCMAKE_OSX_ARCHITECTURES="i386;ppc" -DCMAKE_BUILD_TYPE="Release" -DCMAKE_INSTALL_PREFIX="/tmp" ..
	make
	if [ `uname -s` = "Darwin" ]; then
		make bundle
	else
		make install
	fi
fi
