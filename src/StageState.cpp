//
// Defeat Me - Fight Against Yourself.
// Copyright (C) 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // !HAVE_CONFIG_H
#include "StageState.hpp"
#include <GameStateManager.hpp>
#include <ResourceManager.hpp>
#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include "GameAction.hpp"

using namespace defeatme;

namespace
{
    // The margin between the stage and the screen's edge.
    const size_t MARGIN = 10;
    // The starting position, from the margin.
    const int16_t START = 30;
    // The time the start text should be displayed, in ms.
    const float START_TEXT_TIME = 300.0f;

    ///
    /// @brief Fills a surface's zone with a background color.
    ///
    /// @param[inout] destination The surface to fill in.
    /// @param[in] rect The rectangle to fill @p destination.
    ///
    void
    fillBackground(benzaiten::Surface destination, const SDL_Rect &rect)
    {
        destination.fill(0, 0, 0, SDL_ALPHA_OPAQUE, rect.x, rect.y,
                rect.w, rect.h);
    }
}

BENZAITEN_EVENT_TABLE(StageState)
{
    BENZAITEN_EVENT1(OnActionBegin, &StageState::onActionBegan);
    BENZAITEN_EVENT1(OnActionEnd, &StageState::onActionEnded);
}

StageState::StageState(size_t width, size_t height,
        const benzaiten::ResourceManager &resources):
    backgroundModifications_(),
    bulletsBottomMargin_(height),
    bulletSurface_(resources.graphic("bullet.tga")),
    bulletsTopMargin_(-1 * bulletSurface_.height()),
    enemies_(),
    enemyBullets_(),
    enemyAnimation_(benzaiten::Animation(
                benzaiten::SpriteSheet(resources.graphic("enemy.tga"),
                    16 * SCREEN_SCALE, 14 * SCREEN_SCALE),
                0, 2, 5)),
    gameOver_(resources.graphic("gameover.tga")),
    gameOverY_(0),
    height_(height),
    isGameOver_(true), // Yes, we start as over...
#if defined (GP2X)
    lastAction_(0),
#endif // GP2X
    marginBottom_(height - MARGIN),
    marginLeft_(MARGIN),
    marginRight_(width - MARGIN),
    marginTop_(MARGIN),
    music_(resources.try_music("bgm.ogg")),
    numbers_(L"0123456789", resources.graphic("numbers.tga")),
    numEnemies_(1),
    player_(benzaiten::Animation(
                benzaiten::SpriteSheet(resources.graphic("player.tga"),
                    15 * SCREEN_SCALE, 20 * SCREEN_SCALE),
                0, 2, 5), width / 2,
            height - MARGIN - START, 2 * SCREEN_SCALE),
    playerBullets_(),
    playerSnapshots_(),
    shoot_(resources.try_sound("shoot.ogg")),
    showStartText_(true),
    showStartTextTime_(START_TEXT_TIME),
    stageNumber_("1"),
    stageText_(resources.graphic("stage.tga")),
    starField_(width, height),
    startText_(resources.graphic("start.tga")),
    width_(width)
{
    // Populate the star field.
    using benzaiten::Surface;
    const size_t numStars = 50 * SCREEN_SCALE;
    starField_.addLayer(numStars, Surface::Color(255, 255, 255), 6);
    starField_.addLayer(numStars, Surface::Color(128, 128, 128), 4);
    starField_.addLayer(numStars, Surface::Color(50, 50, 50), 2);

    // We need to take care of the player's width for the right margin and the
    // height for the bottom.
    marginRight_ -= player_.width();
    marginBottom_ -= player_.height();

    // The game over screen is set to the bottom.
    gameOverY_ = marginBottom_;

    // Add the first level's enemy that doesn't move at all.
    enemies_.push_back(Enemy(enemyAnimation_, width / 2, MARGIN + START));

    // Add the whole stage as a modification, so the first time all gets
    // erased.
    SDL_Rect stage = {0, 0, Uint16(width), Uint16(height)};
    addBackgroundModification(stage);
}

void
StageState::addBackgroundModification(const SDL_Rect &rect)
{
    backgroundModifications_.push_back(rect);
}

void
StageState::addEnemyBullet(int16_t x, int16_t y, float speed)
{
    enemyBullets_.push_back(Bullet(bulletSurface_, x, y,
                Bullet::DownDirection, speed));
}

void
StageState::draw(benzaiten::Surface &screen)
{
    restoreBackground(screen);
    starField_.draw(screen);
    addBackgroundModification(stageText_.blit(MARGIN, MARGIN, screen));
    addBackgroundModification(numbers_.draw(stageNumber_,
                MARGIN + 5 + stageText_.width(), MARGIN, screen));
    BOOST_FOREACH(const Bullet &bullet, enemyBullets_)
    {
        addBackgroundModification(bullet.draw(screen));
    }
    if (isGameOver())
    {
        addBackgroundModification(
                gameOver_.blit(MARGIN, gameOverY_, screen));
    }
    else
    {
        BOOST_FOREACH(const Bullet &bullet, playerBullets_)
        {
            addBackgroundModification(bullet.draw(screen));
        }
        addBackgroundModification(player().draw(screen));
    }
    BOOST_FOREACH(const Enemy &enemy, enemies_)
    {
        if (enemy.isAlive())
        {
            addBackgroundModification(enemy.draw(screen));
        }
    }

    if (showStartText())
    {
        addBackgroundModification(startText_.blit(
                    width_ / 2 - startText_.width() / 2,
                    height_ / 2 - startText_.height() / 2, screen));
    }
}

bool
StageState::isGameOver() const
{
    return isGameOver_;
}

void
StageState::nextLevel()
{
    Enemy::Snapshot start = playerSnapshots_[0];
    enemies_.push_back(Enemy(enemyAnimation_, start.x, start.y,
                playerSnapshots_));
    stageNumber_ = boost::lexical_cast<std::string>(enemies_.size());
    reset();
}

void
StageState::onActionBegan(int action)
{
#if defined (GP2X)
    lastAction_ = action;
#endif // GP2X

    switch(action)
    {
        case Fire:
            if (isGameOver())
            {
                reset();
            }
            else
            {
                player().setIsFiring(true);
            }
            break;

        case Exit:
            stateManager().removeActiveState();
            break;

        BEGIN_MOVE_OF(player(), MoveUp, MoveUpRight, MoveRight,
                MoveDownRight, MoveDown, MoveDownLeft, MoveLeft, MoveUpLeft);

        case Restart:
            if (isGameOver())
            {
                restart();
            }
            break;

        default:
            break;
    }
}

void
StageState::onActionEnded(int action)
{
    switch(action)
    {
        END_MOVE_OF(player(), MoveUp, MoveUpRight, MoveRight, MoveDownRight,
                MoveDown, MoveDownLeft, MoveLeft, MoveUpLeft);

        default:
            break;
    }
}

StageState::PlayerShip &
StageState::player()
{
    return player_;
}

void
StageState::removeEnemy(Enemy &enemy)
{
    enemy.setAlive(false);
    --numEnemies_;
}

void
StageState::reset()
{
    std::for_each(enemies_.begin(), enemies_.end(),
            boost::bind(&Enemy::reset, _1));
    playerSnapshots_.clear();
    player().reset();
    enemyBullets_.clear();
    playerBullets_.clear();
    numEnemies_ = enemies_.size();

    for(int16_t x = bulletSurface_.width() ; x < width_ ; x += bulletSurface_.width() * 2)
    {
        addEnemyBullet(x, 0, (2.0f / (1.0f + enemies_.size())) * SCREEN_SCALE);
    }

    if (isGameOver())
    {
        setGameOver(false);
        music_->play();
    }
}

void
StageState::restart()
{
    enemies_.clear();
    enemies_.push_back(Enemy(enemyAnimation_, width_ / 2, MARGIN + START));
    stageNumber_ = "1";
    reset();
}

void
StageState::restoreBackground(benzaiten::Surface &screen)
{
    std::for_each(backgroundModifications_.begin(),
            backgroundModifications_.end(),
            boost::bind(&fillBackground, screen, _1));
    backgroundModifications_.clear();
}

void
StageState::setGameOver(bool isOver)
{
    isGameOver_ = isOver;
    if (isOver)
    {
        music_->halt();
        gameOverY_ = height_;
        showStartTextTime_ = START_TEXT_TIME;
        showStartText_ = true;
    }
}

bool
StageState::showStartText() const
{
    return isGameOver() && showStartText_;
}

void
StageState::update(float elapsedTime)
{
    starField_.update(elapsedTime);

    playerSnapshots_.push_back(Enemy::Snapshot(player().x(),
                marginTop_ + marginBottom_ - player().y(),
                player().isFiring()));

    if ( !isGameOver() )
    {
        if (player().isFiring())
        {
            playerBullets_.push_back(Bullet(bulletSurface_, player().x(),
                        player().y(), Bullet::UpDirection, 3 * SCREEN_SCALE));
            player().setIsFiring(false);
            shoot_->play();
        }

        for(std::list<Bullet>::iterator bullet = playerBullets_.begin() ;
                bullet != playerBullets_.end() ; )
        {
            bool removeBullet = false;
            BOOST_FOREACH(Enemy &enemy, enemies_)
            {
                if(enemy.isAlive() && areColliding(*bullet, enemy))
                {
                    removeEnemy(enemy);
                    removeBullet = true;
                    break;
                }
            }

            if (!removeBullet)
            {
                bullet->update(elapsedTime);
                removeBullet = bullet->y() < bulletsTopMargin_;
            }

            if (removeBullet)
            {
                bullet = playerBullets_.erase(bullet);
            }
            else
            {
                ++bullet;
            }
        }
    }
    for(std::list<Bullet>::iterator bullet = enemyBullets_.begin() ;
            bullet != enemyBullets_.end() ; )
    {
        bool removeBullet = false;
        bullet->update(elapsedTime);
        if (!isGameOver() && areColliding(*bullet, player()))
        {
            player().setAlive(false);
            removeBullet = true;
        }

        if (removeBullet || bullet->y() > bulletsBottomMargin_)
        {
            bullet = enemyBullets_.erase(bullet);
        }
        else
        {
            ++bullet;
        }
    }

    player().update(elapsedTime, marginLeft_, marginRight_, marginTop_,
            marginBottom_);

    BOOST_FOREACH(Enemy &enemy, enemies_)
    {
        if (enemy.isAlive())
        {
            enemy.nextFrame();
            enemy.update(elapsedTime);
            if (enemy.isFiring())
            {
                addEnemyBullet(enemy.x(), enemy.y(), 3.0f * SCREEN_SCALE);
                shoot_->play();
            }
        }
    }

    if (isGameOver())
    {
        showStartTextTime_ -= elapsedTime;
        if (showStartTextTime_ <= 0.0f)
        {
            showStartText_ = !showStartText_;
            showStartTextTime_ = START_TEXT_TIME;
        }
        if (gameOverY_ > marginBottom_)
        {
            gameOverY_ -= 1;
        }
    }
    else
    {
        if (!player().isAlive())
        {
            setGameOver(true);
        }
        else if (0 == numEnemies_)
        {
            nextLevel();
        }
    }
}
