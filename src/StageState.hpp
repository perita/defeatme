//
// Defeat Me - Fight Against Yourself.
// Copyright (C) 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_DEFEAT_ME_STATE_STATE_HPP)
#define GEISHA_STUDIOS_DEFEAT_ME_STATE_STATE_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <list>
#include <vector>
#include <BitmapFont.hpp>
#include <EightWayController.hpp>
#include <IGameState.hpp>
#include <Music.hpp>
#include <Sound.hpp>
#include <StarField.hpp>
#include "Bullet.hpp"
#include "Enemy.hpp"
#include "Player.hpp"

// Forward declarations.
namespace benzaiten
{
    class ResourceManager;
}

namespace defeatme
{
    ///
    /// @class StageState
    /// @brief Controls every game's stage.
    ///
    class StageState: public benzaiten::IGameState
    {
        public:
            ///
            /// @brief Constructor.
            ///
            /// @param[in] width The width of the stage.
            /// @param[in] height The height of the stage.
            /// @param[in] resources The resource manager to use to get the
            ///            resources from.
            ///
            StageState(size_t width, size_t height,
                    const benzaiten::ResourceManager &resources);

            virtual void draw(benzaiten::Surface &screen);
            virtual void update(float elapsedTime);

        private:
            typedef benzaiten::EightWayController<Player> PlayerShip;

            ///
            /// @brief Adds a new background modification.
            ///
            /// @param[in] rect The rectangle the background was modified.
            ///
            void addBackgroundModification(const SDL_Rect &rect);

            ///
            /// @brief Adds a new enemy bullet.
            ///
            /// @param[in] x The horizontal position to add the bullet to.
            /// @param[in] y The vertical position to add the bullet to.
            /// @param[in] speed The bullet's speed.
            ///
            void addEnemyBullet(int16_t x, int16_t y, float speed);

            ///
            /// @brief Tells whether the game is over.
            ///
            /// @return @c true if the game is over.
            ///
            bool isGameOver() const;

            ///
            /// @brief Moves to the next level.
            ///
            void nextLevel();

            ///
            /// @brief A game action began.
            ///
            /// @param[in] action The action that began.
            ///
            void onActionBegan(int action);

            ///
            /// @brief A game action ended.
            ///
            /// @param[in] action The action that ended.
            ///
            void onActionEnded(int action);

            ///
            /// @brief Gets the reference to the player.
            ///
            /// @return The reference to the player.
            ///
            PlayerShip &player();

            ///
            /// @brief Removes an enemy,
            ///
            /// @param[in] enemy The enemy to remove.
            ///
            void removeEnemy(Enemy &enemy);

            ///
            /// @brief Resets the stage.
            ///
            void reset();

            ///
            /// @brief Restarts from the first level.
            ///
            void restart();

            ///
            /// @brief Brief restores all background's modifications.
            ///
            /// @param[inout] screen The surface to restore the background's
            ///               modifications to.
            ///
            void restoreBackground(benzaiten::Surface &screen);

            ///
            /// @brief Sets whether the game is over.
            ///
            /// @param[in] isOver @c true if the game is over.
            ///
            void setGameOver(bool isOver);

            ///
            /// @brief Tells whether to show the starting text.
            ///
            /// @return @c true if the text is to be shown.
            ///
            bool showStartText() const;

            /// The vector with all background modifications.
            std::vector<SDL_Rect> backgroundModifications_;
            /// The bottom margin for bullets.
            int16_t bulletsBottomMargin_;
            /// The surface to use for bullets.
            benzaiten::Surface bulletSurface_;
            /// The top margin for bullets.
            int16_t bulletsTopMargin_;
            /// The list of enemies.
            std::vector<Enemy> enemies_;
            /// The list of enemies' bullets.
            std::list<Bullet> enemyBullets_;
            /// The animation to use for enemies.
            benzaiten::Animation enemyAnimation_;
            /// The game over text.
            benzaiten::Surface gameOver_;
            /// The position of the game over.
            int16_t gameOverY_;
            /// The stage's height.
            size_t height_;
            /// Tells whether is game over.
            bool isGameOver_;
#if defined (GP2X)
            /// The last action performed.
            int lastAction_;
#endif // GP2X
            /// The margin at the bottom.
            int16_t marginBottom_;
            /// The margin at the left.
            int16_t marginLeft_;
            /// The margin at the right.
            int16_t marginRight_;
            /// The margin at top.
            int16_t marginTop_;
            /// The background music.
            benzaiten::Music::ptr music_;
            /// The numbers font.
            benzaiten::BitmapFont numbers_;
            /// The number of enemies remainig.
            size_t numEnemies_;
            /// The player.
            PlayerShip player_;
            /// The list of player's bullets.
            std::list<Bullet> playerBullets_;
            /// The snapshots of the player.
            Enemy::Snapshots playerSnapshots_;
            /// The sound of a shooting.
            benzaiten::Sound::ptr shoot_;
            /// Tells whether to show the start text.
            bool showStartText_;
            /// The time to toggle the show start text.
            float showStartTextTime_;
            /// The "number" of the stage as text.
            std::string stageNumber_;
            /// The text with "stage".
            benzaiten::Surface stageText_;
            /// The background's star field.
            benzaiten::StarField starField_;
            /// The starting text.
            benzaiten::Surface startText_;
            /// The stage's width.
            size_t width_;

            DECLARE_BENZAITEN_EVENT_TABLE();
    };
}

#endif // !GEISHA_STUDIOS_DEFEAT_ME_STATE_STATE_HPP
