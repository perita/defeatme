//
// Defeat Me - Fight Against Yourself.
// Copyright (C) 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // !HAVE_CONFIG_H
#include "Enemy.hpp"
#include <cassert>

using namespace defeatme;

Enemy::Enemy(const benzaiten::Animation &animation, int16_t x, int16_t y,
        const Snapshots &snapshots):
    CollisionableSprite(animation, x - animation.width() / 2, y, 2 * SCREEN_SCALE, 2 * SCREEN_SCALE),
    currentSnapshot_(0),
    snapshots_(snapshots)
{
    if (snapshots_.empty())
    {
        snapshots_.push_back(Snapshot(this->x(), this->y(), false));
    }
}

bool
Enemy::isFiring() const
{
    assert(!snapshots_.empty());
    assert(currentSnapshot_ < snapshots_.size());
    return snapshots_[currentSnapshot_].firing;
}

void
Enemy::reset()
{
    setAlive(true);
    currentSnapshot_ = 0;
}

void
Enemy::update(float elaspedTime)
{
    assert(!snapshots_.empty());
    ++currentSnapshot_;
    if (snapshots_.size() == currentSnapshot_)
    {
        currentSnapshot_ = 0;
    }
    assert(currentSnapshot_ < snapshots_.size());
    setX(snapshots_[currentSnapshot_].x);
    setY(snapshots_[currentSnapshot_].y);
}
