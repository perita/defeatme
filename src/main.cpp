//
// Defeat Me - Fight Against Yourself.
// Copyright (C) 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // !HAVE_CONFIG_H
#include <cstdlib>
#include <iostream>
#include <stdexcept>
#include <Audio.hpp>
#include <Game.hpp>
#include <GameStateManager.hpp>
#include <LogoState.hpp>
#include <ResourceManager.hpp>
#include <SDL_main.h>
#include <System.hpp>
#include <boost/bind.hpp>
#include "GameAction.hpp"
#include "StageState.hpp"

using namespace benzaiten;
using namespace defeatme;

namespace
{
    const size_t SCREEN_HEIGHT = 240 * SCREEN_SCALE;
    const size_t SCREEN_WIDTH = 320 * SCREEN_SCALE;
}

int
main(int argc, char *argv[])
{
    try
    {
        System system;
        ResourceManager resources("defeatme");
        system.setIcon(resources, "icon.tga");

#if SCREEN_SCALE > 1
        resources.setGraphicScaleFunction(
            boost::bind(benzaiten::fastScale, _1, SCREEN_SCALE));
#endif // SCALE_FACTOR > 1

        Surface screen(system.setVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT));
        system.setTitle("Defeat Me!");


        Audio audio(44100, Audio::S16SYS, Audio::Stereo, 1024);

        GameStateManager gameStateManager;
        gameStateManager.setActiveState(IGameState::ptr(
                    new StageState(SCREEN_WIDTH, SCREEN_HEIGHT, resources)));
        gameStateManager.setActiveState(IGameState::ptr(
                    new LogoState(resources.graphic("logo.tga"))));

        EventManager eventManager;
        eventManager.mapKey(SDLK_RETURN, Fire);
        eventManager.mapKey(SDLK_SPACE, Fire);
        eventManager.mapKey(SDLK_LSHIFT, Fire);
        eventManager.mapKey(SDLK_RSHIFT, Fire);
        eventManager.mapKey(SDLK_ESCAPE, Exit);
        eventManager.mapKey(SDLK_DOWN, MoveDown);
        eventManager.mapKey(SDLK_j, MoveDown);
        eventManager.mapKey(SDLK_s, MoveDown);
        eventManager.mapKey(SDLK_LEFT, MoveLeft);
        eventManager.mapKey(SDLK_h, MoveLeft);
        eventManager.mapKey(SDLK_a, MoveLeft);
        eventManager.mapKey(SDLK_RIGHT, MoveRight);
        eventManager.mapKey(SDLK_l, MoveRight);
        eventManager.mapKey(SDLK_d, MoveRight);
        eventManager.mapKey(SDLK_UP, MoveUp);
        eventManager.mapKey(SDLK_k, MoveUp);
        eventManager.mapKey(SDLK_w, MoveUp);
        eventManager.mapKey(SDLK_r, Restart);

#if defined(A320)
        eventManager.mapKey(A320_BUTTON_A, Fire);
        eventManager.mapKey(A320_BUTTON_B, Fire);
        eventManager.mapKey(A320_BUTTON_R, Restart);
#endif // A320

#if defined(GP2X)
        eventManager.mapJoy(GP2X_BUTTON_A, Fire);
        eventManager.mapJoy(GP2X_BUTTON_B, Fire);
        eventManager.mapJoy(GP2X_BUTTON_DOWN, MoveDown);
        eventManager.mapJoy(GP2X_BUTTON_DOWNLEFT, MoveDownLeft);
        eventManager.mapJoy(GP2X_BUTTON_DOWNRIGHT, MoveDownRight);
        eventManager.mapJoy(GP2X_BUTTON_L, Exit);
        eventManager.mapJoy(GP2X_BUTTON_LEFT, MoveLeft);
        eventManager.mapJoy(GP2X_BUTTON_RIGHT, MoveRight);
        eventManager.mapJoy(GP2X_BUTTON_SELECT, Restart);
        eventManager.mapJoy(GP2X_BUTTON_UP, MoveUp);
        eventManager.mapJoy(GP2X_BUTTON_UPLEFT, MoveUpLeft);
        eventManager.mapJoy(GP2X_BUTTON_UPRIGHT, MoveUpRight);
        eventManager.mapJoy(GP2X_BUTTON_X, Fire);
        eventManager.mapJoy(GP2X_BUTTON_Y, Fire);
#else // !GP2X
        eventManager.mapJoy(0, Fire);
        eventManager.mapJoy(1, Fire);
        eventManager.mapJoy(2, Fire);
        eventManager.mapJoy(3, Fire);
        eventManager.mapJoy(7, Restart);
        eventManager.mapJoyAxis(AxisDown, MoveDown);
        eventManager.mapJoyAxis(AxisLeft, MoveLeft);
        eventManager.mapJoyAxis(AxisRight, MoveRight);
        eventManager.mapJoyAxis(AxisUp, MoveUp);
#endif // GP2X

        Game game(eventManager, gameStateManager, screen);
        game.run();
        return EXIT_SUCCESS;
    }
    catch(std::exception &e)
    {
        std::cerr << "Fatal error: " << e.what() << std::endl;
    }
    catch(...)
    {
        std::cerr << "Unknown error" << std::endl;
    }

    return EXIT_FAILURE;
}
