//
// Defeat Me - Fight Against Yourself.
// Copyright (C) 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // !HAVE_CONFIG_H
#include "CollisionableSprite.hpp"

using namespace defeatme;


CollisionableSprite::CollisionableSprite(const benzaiten::Animation &animation,
        int16_t x, int16_t y, size_t marginX, size_t marginY):
    Sprite(animation),
    alive_(true),
    boundingBox_(),
    offsetX_(marginX),
    offsetY_(marginY),
    x_(0),
    y_(0)
{
    setX(x);
    setY(y);

    boundingBox_.w = animation.width() - 2 * marginX;
    boundingBox_.h = animation.height() - 2 * marginY;
}

CollisionableSprite::CollisionableSprite(const benzaiten::Surface &surface,
        int16_t x, int16_t y, size_t marginX, size_t marginY):
    Sprite(surface),
    alive_(true),
    boundingBox_(),
    offsetX_(marginX),
    offsetY_(marginY),
    x_(0),
    y_(0)
{
    setX(x);
    setY(y);

    boundingBox_.w = surface.width() - 2 * marginX;
    boundingBox_.h = surface.height() - 2 * marginY;
}

SDL_Rect
CollisionableSprite::boundingBox() const
{
    return boundingBox_;
}

SDL_Rect
CollisionableSprite::draw(benzaiten::Surface &screen) const
{
    return draw(x(), y(), screen);
}

bool
CollisionableSprite::isAlive() const
{
    return alive_;
}

void
CollisionableSprite::setAlive(bool alive)
{
    alive_ = alive;
}

void
CollisionableSprite::setX(int16_t x)
{
    x_ = x;
    boundingBox_.x = x + offsetX_;
}

void
CollisionableSprite::setY(int16_t y)
{
    y_ = y;
    boundingBox_.y = y + offsetY_;
}

int16_t
CollisionableSprite::x() const
{
    return x_;
}

int16_t
CollisionableSprite::y() const
{
    return y_;
}

namespace defeatme
{
    bool
    areColliding(const CollisionableSprite &one,
            const CollisionableSprite &other)
    {
        SDL_Rect oneBB = one.boundingBox();
        SDL_Rect otherBB = other.boundingBox();

        if ( (static_cast<int16_t>(oneBB.y + oneBB.h) < otherBB.y) ||
            (oneBB.x > static_cast<int16_t>(otherBB.x + otherBB.w)) ||
            (static_cast<int16_t>(oneBB.x + oneBB.w) < otherBB.x) ||
            (oneBB.y > static_cast<int16_t>(otherBB.y + otherBB.h)) )
        {
            return false;
        }
        return true;
    }
}
