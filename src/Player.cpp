//
// Defeat Me - Fight Against Yourself.
// Copyright (C) 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // !HAVE_CONFIG_H
#include "Player.hpp"

using namespace defeatme;

Player::Player(const benzaiten::Animation &animation, int16_t x, int16_t y,
        uint8_t speed):
    CollisionableSprite(animation, x - animation.width() / 2, y - animation.height(), 3 * SCREEN_SCALE, 3 * SCREEN_SCALE),
    initialX_(this->x()),
    initialY_(this->y()),
    isFiring_(false),
    speed_(speed)
{
}

bool
Player::isFiring() const
{
    return isFiring_;
}

void
Player::reset()
{
    setAlive(true);
    setIsFiring(false);
    setX(initialX_);
    setY(initialY_);
}

void
Player::setIsFiring(bool firing)
{
    isFiring_ = firing;
}

uint8_t
Player::speed() const
{
    return speed_;
}

void
Player::update(float elapsedTime)
{
    nextFrame();
}
