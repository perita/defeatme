//
// Defeat Me - Fight Against Yourself.
// Copyright (C) 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined(GEISHA_STUDIOS_DEFEAT_ME_PLAYER_HPP)
#define GEISHA_STUDIOS_DEFEAT_ME_PLAYER_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include "CollisionableSprite.hpp"

namespace defeatme
{
    ///
    /// @class Player
    /// @brief The player on the stage.
    ///
    class Player: public CollisionableSprite
    {
        public:
            typedef int8_t Direction;

            ///
            /// @brief Constructor.
            ///
            /// @param[in] animation The animation to use as the sprite image.
            /// @param[in] x The initial X position to center the ship on.
            /// @param[in] y The initial Y position to center the ship on.
            /// @param[in] speed The speed in which the ship moves.
            ///
            Player(const benzaiten::Animation &animation, int16_t x, int16_t y,
                    uint8_t speed);

            ///
            /// @brief Tells whether the player is firing or not.
            ///
            /// @return @c true if the player is firing.
            ///
            bool isFiring() const;

            ///
            /// @brief Resets the player to its initial position.
            ///
            void reset();

            ///
            /// @brief Sets whether the player if firing.
            ///
            /// @param[in] firing Set to @c true to set the player in
            ///            "firing mode".
            ///
            void setIsFiring(bool firing);

            ///
            /// @brief Updates the ship's position based on the direction.
            ///
            /// @param[in] elapsedTime The time elapsed since the last call
            ///            to this function.
            ///
            void update(float elapsedTime);

        protected:
            ///
            /// @brief Gets the current ship's speed.
            ///
            /// @return The ship's speed.
            ///
            uint8_t speed() const;

        private:
            /// The initial X position.
            int16_t initialX_;
            /// The initial Y position.
            int16_t initialY_;
            /// Tells whether is firing.
            bool isFiring_;
            /// The speed of the ship.
            uint8_t speed_;
    };
}

#endif // !GEISHA_STUDIOS_DEFEAT_ME_PLAYER_HPP
