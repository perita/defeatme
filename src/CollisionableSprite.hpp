//
// Defeat Me - Fight Against Yourself.
// Copyright (C) 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined(GEISHA_STUDIOS_DEFEAT_ME_COLLISIONABLE_SPRITE_HPP)
#define GEISHA_STUDIOS_DEFEAT_ME_COLLISIONABLE_SPRITE_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <Sprite.hpp>

namespace defeatme
{
    ///
    /// @class CollisionableSprite
    /// @brief An sprite that can collide with another one.
    ///
    class CollisionableSprite: public benzaiten::Sprite
    {
        public:
            using benzaiten::Sprite::draw;

            ///
            /// @brief Constructor.
            ///
            /// @param[in] animation The animation to use.
            /// @param[in] x The initial X position.
            /// @param[in] y The initial Y position.
            /// @param[in] marginX The margin in X for the bounding box.
            /// @param[in] marginY The margin in Y for the bounding box.
            ///
            CollisionableSprite(const benzaiten::Animation &animation, int16_t x,
                    int16_t y, size_t marginX = 0, size_t marginY = 0);

            ///
            /// @brief Constructor.
            ///
            /// @param[in] surface The surface to get the sprite's image from.
            /// @param[in] x The initial X position.
            /// @param[in] y The initial Y position.
            /// @param[in] marginX The margin in X for the bounding box.
            /// @param[in] marginY The margin in Y for the bounding box.
            ///
            CollisionableSprite(const benzaiten::Surface &surface, int16_t x,
                    int16_t y, size_t marginX = 0, size_t marginY = 0);

            ///
            /// @brief The sprite's bounding box.
            ///
            /// @return The sprite's bounding box.
            ///
            SDL_Rect boundingBox() const;

            ///
            /// @brief Draws the sprite in its current position.
            ///
            /// @param[in] screen The surface where to draw the sprite to.
            ///
            /// @return The rectangle where the sprite has been drawn to.
            ///
            SDL_Rect draw(benzaiten::Surface &screen) const;

            ///
            /// @brief Tells whether the sprite is alive.
            ///
            /// @return @c true if the sprite is alive.
            ///
            bool isAlive() const;

            ///
            /// @brief The current position in the horizontal axis.
            ///
            /// @return The position in X.
            ///
            int16_t x() const;

            ///
            /// @brief The current position in the vertical axis.
            ///
            /// @return The position in Y.
            ///
            int16_t y() const;

            ///
            /// @brief Sets whether the sprite is alive.
            ///
            /// @param[in] alive Set to @c true to mark the sprite alive.
            ///
            void setAlive(bool alive);

            ///
            /// @brief Sets the horizontal position.
            ///
            /// @param[in] x The horizontal position to set.
            ///
            void setX(int16_t x);

            ///
            /// @brief Sets the vertical position.
            ///
            /// @param[in] y The vertical position to set.
            ///
            void setY(int16_t y);

        private:
            /// Whether the sprite is alive.
            bool alive_;
            /// The sprite's bounding box.
            SDL_Rect boundingBox_;
            /// The offset in X of the bounding box.
            size_t offsetX_;
            /// The offset in Y of the bounding box.
            size_t offsetY_;
            /// The current horizontal position.
            int16_t x_;
            /// The current vertical position.
            int16_t y_;
    };

    ///
    /// @brief Tells whether two collisionable sprites are colliding.
    ///
    /// @param[in] one The first sprite to check whether is colliding.
    /// @param[in] other The other sprite to check whether is colliding.
    ///
    /// @return @c true if @p one and @p other are colliding, @p false
    ///         otherwise.
    ///
    bool areColliding(const CollisionableSprite &one,
            const CollisionableSprite &other);
}

#endif // GEISHA_STUDIOS_DEFEAT_ME_COLLISIONABLE_SPRITE_HPP
