#include <windows.h>
LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US
#pragma code_page(1252)

A ICON "@PROJECT_SOURCE_DIR@/share/defeatme/gfx/defeatme.ico"
__GDF_XML DATA "@CMAKE_CURRENT_SOURCE_DIR@/defeatme.gdf"
__GDF_THUMBNAIL DATA "@PROJECT_SOURCE_DIR@/share/defeatme/gfx/defeatme.png"

VS_VERSION_INFO VERSIONINFO
    FILEVERSION @VERSION_MAJOR@, @VERSION_MINOR@, @VERSION_PATCH@, @VERSION_REV@
    PRODUCTVERSION @VERSION_MAJOR@, @VERSION_MINOR@, @VERSION_PATCH@, @VERSION_REV@
    FILEFLAGSMASK VS_FFI_FILEFLAGSMASK
#if defined(_DEBUG)
    FILEFLAGS VS_FF_DEBUG
#else // !_DEBUG
    FILEFLAGS 0
#endif // _DEBUG
    FILEOS VOS__WINDOWS32
    FILETYPE VFT_APP
    FILESUBTYPE VFT_UNKNOWN
    BEGIN
        BLOCK "StringFileInfo"
        BEGIN
            BLOCK "040904b0"
            BEGIN
                VALUE "CompanyName", "Geisha Studios"
                VALUE "FileDescription", "Fight against yourself"
                VALUE "FileVersion", "@VERSION@"
                VALUE "InternalName", "@PROJECT_NAME@"
                VALUE "LegalCopyright", "Copyright (c) 2009, 2010 Geisha Studios"
                VALUE "OriginalFileName", "defeatme.exe"
                VALUE "ProductName", "@MACOSX_BUNDLE_BUNDLE_NAME@"
                VALUE "ProductVersion", "@VERSION@"
            END
        END
        BLOCK "VarFileInfo"
        BEGIN
            VALUE "Translation", 0x409, 1200
        END
    END
