//
// Defeat Me - Fight Against Yourself.
// Copyright (C) 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined(GEISHA_STUDIOS_DEFEAT_ME_BULLET_HPP)
#define GEISHA_STUDIOS_DEFEAT_ME_BULLET_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include "CollisionableSprite.hpp"

namespace defeatme
{
    ///
    /// @class Bullet
    /// @brief Either a player's or enemy's bullet moving in line.
    ///
    class Bullet: public CollisionableSprite
    {
        public:
            enum Direction
            {
                DownDirection = 1,
                UpDirection = -1
            };

            ///
            /// @brief Constructor.
            ///
            /// @param[in] surface The surface to get the sprite's image from.
            /// @param[in] x The initial X position.
            /// @param[in] y The initial Y position.
            /// @param[in] direction The direction to move the bullet to.
            /// @param[in] speed The speed in which the bullet travels.
            ///
            Bullet(const benzaiten::Surface &surface, int16_t x, int16_t y,
                    Direction direction, float speed);

            ///
            /// @brief Updates the bullet's position.
            ///
            /// @param[in] elapsedTime The time elapsed since the last
            ///            call to this function, in ms.
            ///
            void update(float elapsedTime);

        private:
            /// The speed in which to move the bullet to.
            float speed_;
            /// The current Y position.
            float y_;
    };
}

#endif // GEISHA_STUDIOS_DEFEAT_ME_BULLET_HPP
