//
// Defeat Me - Fight Against Yourself.
// Copyright (C) 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined(GEISHA_STUDIOS_DEFEAT_ME_ENEMY_HPP)
#define GEISHA_STUDIOS_DEFEAT_ME_ENEMY_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <utility>
#include <vector>
#include "CollisionableSprite.hpp"

namespace defeatme
{
    ///
    /// @class Enemy
    /// @brief A single enemy on the stage.
    ///
    class Enemy: public CollisionableSprite
    {
        public:
            ///
            /// @class Snapshot
            /// @brief A position and firing snapshot.
            ///
            struct Snapshot
            {
                ///
                /// @brief Constructor.
                ///
                /// @param[in] x The horizontal position.
                /// @param[in] y The vertical position.
                /// @param[in] firing Tells whether the enemy is firing.
                ///
                Snapshot(int16_t x, int16_t y, bool firing):
                    firing(firing),
                    x(x),
                    y(y)
                {
                }

                /// Tells whether the enemy is firing.
                bool firing;
                /// The horizontal position.
                int16_t x;
                /// The vertical position.
                int16_t y;
            };
            typedef std::vector<Snapshot> Snapshots;

            ///
            /// @brief Constructor.
            ///
            /// @param[in] animation The animation to use as the sprite's image.
            /// @param[in] x The initial X position to center the enemey on.
            /// @param[in] y The initial Y position to center the enemey on.
            /// @param[in] snapshots The snapshots where the enemy must be
            ///            cyclically.
            ///
            Enemy(const benzaiten::Animation &animation, int16_t x, int16_t y,
                    const Snapshots &snapshots = Snapshots());

            ///
            /// @brief Tells whether the enemy is firing.
            ///
            /// @return @c true if the enemy is firing.
            ///
            bool isFiring() const;

            ///
            /// @brief Resets the enemy to its initial state.
            ///
            void reset();

            ///
            /// @brief Updates the enemies positions.
            ///
            void update(float elapsedTime);

        private:
            /// The current snapshot.
            Snapshots::size_type currentSnapshot_;
            /// The positions to move to.
            Snapshots snapshots_;
    };
}

#endif // !GEISHA_STUDIOS_DEFEAT_ME_ENEMY_HPP
